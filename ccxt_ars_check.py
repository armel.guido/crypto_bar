#!/usr/bin/python3

import ccxt

for exchange_id in ccxt.exchanges:
    try:
        exchange = getattr(ccxt, exchange_id)()
        divisas = exchange.load_markets()
        if "BTC/ARS" in divisas.keys():
            print(exchange.__str__)
    except:
        pass
