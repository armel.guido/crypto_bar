#!/usr/bin/python3

from config import engine

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, DateTime, Float, Integer, String

engine = create_engine(engine, echo=True)

Base = declarative_base()


class Crypto(Base):
    __tablename__ = 'crypto'
    
    id = Column(Integer, primary_key=True)
    symbol = Column(String(10))
    open = Column(Float)
    highest = Column(Float)
    lowest = Column(Float)
    closing = Column(Float)
    volume = Column(Float)
    fetch = Column(DateTime)
    def __repr__(self):
        return f"Crypto(id={self.id!r}, symbol={self.symbol!r}, closing={self.closing!r}, volume={self.volume!r})"

Base.metadata.create_all(engine)
