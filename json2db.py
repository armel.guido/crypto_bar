#!/usr/bin/python3

import ccxt
import json
from datetime import datetime
from sqlalchemy.orm import sessionmaker

from db_creator import engine, Crypto

symbols = ["BTC/USDC", "BTC/ARS"]

def crypto_dump(symbol, time, last, limit):
    buda = ccxt.buda()
    if not last:
        info = buda.fetch_ohlcv(symbol, time, last, limit) # buda
        #info = json.dumps(buda.fetch_ohlcv(symbol, time)) # buda
        #with open(f"{symbol}_{time}_{last}.json", "w") as f:
        #    f.write(info)
        #    f.close()
    else:
        info = buda.fetch_ohlcv(symbol, time, last, limit)
    return info

def fifty_m_json2psql(symbol, session, jsonfile):
    for fifty_m in jsonfile:
        Open = fifty_m[1] # (O)pen price, float
        Highest = fifty_m[2] # (H)ighest price, float
        Lowest = fifty_m[3] # (L)owest price, float
        Closing = fifty_m[4] # (C)losing price, float
        Volume = fifty_m[0] # (V)olume (in terms of the base currency), float
        fetch = datetime.fromtimestamp(fifty_m[0]/1000.0) # UTC timestamp in milliseconds, integer 2 datetime()
        crypto = Crypto(symbol=symbol, open=Open, highest=Highest, lowest=Lowest, closing=Closing, volume=Volume, fetch=fetch)
        session.add(crypto)
        # session.flush()
        session.commit()

Session = sessionmaker(bind=engine)
session = Session()

for sym in symbols:
    #start_date = int(datetime(2016, 1, 1, 10, 20).timestamp() * 1000)
    start_date = session.query(Crypto).order_by(Crypto.fetch)[-1]
    start_date = int(datetime.timestamp(start_date.fetch)) * 1000
    jsonfile = crypto_dump(sym, "5m", start_date, 500)
    fifty_m_json2psql(sym, session, jsonfile)
