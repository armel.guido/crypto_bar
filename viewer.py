#!/usr/bin/python3

from sqlalchemy.orm import sessionmaker
from db_creator import engine, Crypto

from datetime import datetime

session = sessionmaker(bind=engine)
session = session()

rows = session.query(Crypto).order_by(Crypto.fetch)

fechas, time, volumen, abierto, alto, bajo, cerrando = [], [], [], [], [], [], []
for row in rows:
    if row.symbol == "BTC/USDC":
        fechas.append(row.fetch)
        time.append(int(datetime.timestamp((row.fetch))) * 1000)
        volumen.append(row.volume)
        abierto.append(row.open)
        alto.append(row.highest)
        bajo.append(row.lowest)
        cerrando.append(row.closing)

import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import ScalarFormatter


years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y-%m-%d') # -%m-%d')

fig, ax = plt.subplots()
ax.plot(fechas, alto, label="alto")
ax.plot(fechas, bajo, label="bajo")
#ax.plot(fechas, volumen, label="volumen")
ax.plot(fechas, cerrando, label="cerrando")

# format the ticks
ax.xaxis.set_major_formatter(yearsFmt)

datemin = fechas[0]
datemax = fechas[-1]
ax.set_xlim(datemin, datemax)

# format the coords message box
ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
ax.grid(True)
ax.legend();
# rotates and right aligns the x labels, and moves the bottom of the
# axes up to make room for them
fig.autofmt_xdate()

#ax.set_xscale('log')

plt.show()
